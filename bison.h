#ifndef BISON_H
#define BISON_H

#include "ast.h"

struct YYLTYPE;

int yylex (void);
void yyerror (char const *);
XmlNode makeLocation(struct XmlContext* context, struct YYLTYPE* src);

//#define SOURCE(NODE,LOC) insertXmlNode(XML_CONTEXT,(NODE),makeLocation(XML_CONTEXT,&(LOC)))
#define SOURCE(NODE,LOC)

#endif
