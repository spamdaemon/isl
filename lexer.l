
%option noyywrap

%{
#include "ast.h"
#include "grammar.tab.h"
#include "string.h"
#include "stdio.h"

int offset=0;
int nesting=0;
extern YYLTYPE yylloc;

#define YY_USER_ACTION         \
if (offset==0) {             \
yylloc.last_line = yylloc.first_line=1; \
yylloc.last_column = yylloc.first_column=1; \
} \
yylloc.first_line = yylloc.last_line; \
yylloc.first_column = yylloc.last_column; \
yylloc.last_line = yylloc.first_line; \
yylloc.last_column = yylloc.first_column+yyleng; \
offset += yyleng; 

%}

%x comment interp
%s expr

%%

"/*"                     { nesting=1; BEGIN(comment); }
<comment>"/*"            { ++nesting; }
<comment>"*/"            { if (--nesting == 0) { BEGIN(INITIAL); } }
<comment>.               { }
<INITIAL,expr,comment>\n      { /* IGNORE */ ++yylloc.last_line; yylloc.last_column=1; }

<expr>"}}"                 { BEGIN(interp); return END_EXPR; }

<interp>\n                 { yylval=XML_TEXT(yytext,yyleng);  ++yylloc.last_line; yylloc.last_column=1;return LITERAL_STRING;   }
<interp>"{{"               { BEGIN(expr); return BEGIN_EXPR; }
<interp>"'''"              { BEGIN(INITIAL); return END_TEXT; }
<interp>[^{']*               { yylval=XML_TEXT(yytext,yyleng);return LITERAL_STRING; }
<interp>['{']              { yylval=XML_TEXT(yytext,yyleng); return LITERAL_STRING; }
<INITIAL>"'''"                      { BEGIN(interp); return BEGIN_TEXT; }
std                      { yylval = XML_TEXT(yytext,0); return STD;  }
import               { return IMPORT;  }
unit                    { return UNIT; }
fn                    { return FUNCTION; }
proc                    { return PROCEDURE; }
mutable                    { return MUTABLE; }
safe                   { return SAFE; }
unsafe                 { return UNSAFE; }
pure                   { return PURE; }
generic                   { return GENERIC; }
var                    { return VAR; }
typeof                 { return TYPEOF; }
type                   { return TYPE; }
char                   { return TYPE_CHAR; }
integer                { return TYPE_INTEGER; }
boolean                { return TYPE_BOOLEAN; }
real                   { return TYPE_REAL; }
decimal                   { return TYPE_DECIMAL; }
string                 { return TYPE_STRING; }
bit                    { return TYPE_CHAR; }
opt                 { return OPTIONAL; }
list                 { return LIST; }
struct                 { return STRUCT; }
union                 { return UNION; }
tuple                  { return TUPLE; }
enum                   { return ENUM; }
stream                   { return STREAM; }
return                 { return RETURN; }
invariant               { return INVARIANT; }
new                    { return NEW; }
for                     { return FOR; }
while                     { return WHILE; }
do                     { return DO; }
break                  { return BREAK; }
continue                  { return CONTINUE; }
goto                  { return GOTO; }
throw                 { return ERROR; }
try                   { return TRY; }
catch                 { return CATCH; }
until                  { return UNTIL; }
if                     { return IF; }
else                     { return ELSE; }
switch                  { return SWITCH; }
default                 { return DEFAULT; }
class                    { return CLASS; }
@[a-zA-Z_][a-zA-Z_0-9]*  { yylval = XML_TEXT(yytext+1,0); return ARG_ATTRIBUTE; }
@@[a-zA-Z_][a-zA-Z_0-9]*  { yylval = XML_TEXT(yytext+1,0); return NO_ARG_ATTRIBUTE; }
@                      { return RESULT; }
alias                    { return ALIAS; }
macro                    { return MACRO; }
cast                    { return CAST; }
downcast                { return LOSSY_CAST; }
true|false                   { yylval=XML_TEXT(yytext,0); return LITERAL_BOOLEAN; }
[0-9]                    { yylval=XML_TEXT(yytext,0); return LITERAL_INTEGER; }
[1-9][_0-9]*[0-9]        { yylval=XML_TEXT(yytext,0); return LITERAL_INTEGER; }
0[Xx][a-fA-F0-9_]*[a-fA-F0-9]        { yylval=XML_TEXT(yytext,0); return LITERAL_INTEGER; }
0[bB][01_]*[01]          { yylval=XML_TEXT(yytext,0); return LITERAL_INTEGER; }
0[0-7_]*[0-7]            { yylval=XML_TEXT(yytext,0); return LITERAL_INTEGER; }
[0-9]\.([0-9_]*[0-9])        { yylval=XML_TEXT(yytext,0); return LITERAL_REAL; }
[1-9][0-9_]+\.([0-9_]*[0-9]) { yylval=XML_TEXT(yytext,0); return LITERAL_REAL; }
\.([0-9_]*[0-9])              { yylval=XML_TEXT(yytext,0); return LITERAL_REAL; }

[a-zA-Z](([a-zA-Z0-9_]*[a-zA-Z0-9])?)  { yylval = XML_TEXT(yytext,0); return IDENTIFIER;  }
\"[^\"]*\"              { yylval=XML_TEXT(yytext+1,yyleng-2); return LITERAL_STRING; }
'[^']+'                { yylval=XML_TEXT(yytext+1,yyleng-2); return LITERAL_CHARACTER; }
\/\/.*\n                 { /* COMMENT */ ++yylloc.last_line; yylloc.last_column=1; }
[ \t]                  { /* IGNORE */ }
->                     { return MAP_OPERATOR; }
[+]                    { return PLUS_OPERATOR; }
[\-]                   { return MINUS_OPERATOR; }
[*]                    { return MUL_OPERATOR; }
[/]                    { return DIV_OPERATOR; }
[%]                    { return MOD_OPERATOR; }
>>                     { return SHIFT_RIGHT_OPERATOR; }
>>>                     { return ARITHMETIC_SHIFT_RIGHT_OPERATOR; }
\<\<                     { return SHIFT_LEFT_OPERATOR; }
\<                      {  return LT_OPERATOR; }
>                      { return GT_OPERATOR; }
\<=                     {  return LE_OPERATOR; }
>=                     { return GE_OPERATOR; }
==                     { return EQ_OPERATOR; }
!=                     {  return NE_OPERATOR; }
\&\&                     {return LOGICAL_AND_OPERATOR; }
\|\|                     { return LOGICAL_OR_OPERATOR; }
\^\^                     { return LOGICAL_XOR_OPERATOR; }
!                     { return LOGICAL_NOT_OPERATOR; }
\&                     {  return BITWISE_AND_OPERATOR; }
\|                     { return BITWISE_OR_OPERATOR; }
\^                     { return BITWISE_XOR_OPERATOR; }
~                     { return BITWISE_NOT_OPERATOR; }
[=]                    { return ASSIGN; }
\.\.                 { return RANGE; }
[(){}[\]]              { /** GROUPINGS */ return yytext[0]; }
[,;:.?]                { /* PUNCTUATION */ return yytext[0]; }
<<EOF>>                { yyterminate(); return 0; }
.                      { fprintf(stderr,"%d:%d unexpected character %s\n",yylloc.last_line,yylloc.last_column,yytext); exit(1); }
%%
