#ifndef _TOPL_XML_H
#define _TOPL_XML_H

#include <stdint.h>
#include <stdio.h>


/** An XML node */
typedef size_t XmlNode;

struct XmlContext;

/**
 * Create a context that mananages the consruction and lifetime of xml nodes.
 * @return context
 */
struct XmlContext* createXmlContext();

/**
 * Destroy all nodes maintained by this context.
 * @param context a context
 */
void destroyXmlContext(struct XmlContext* context);

XmlNode renameXmlNode(struct XmlContext* ctx, const char* name, XmlNode node);

/**
 * Create a new XML element.
 * @param context a context
 * @param name the tag name
 * @param ... children and attributes, 0 terminated.
 * @return a new node
 */
XmlNode newXmlElement(struct XmlContext* ctx, const char* name, ...);

/**
 * Create an XML attribute node
 * @param text the text
 * @param n the number of characters (or 0 to use use strlen)
 */
XmlNode newXmlAttribute(struct XmlContext* ctx, const char* name, XmlNode text);

/**
 * Create text xml node.
 * @param text the text
 * @param n the number of characters (or 0 to use use strlen)
 */
XmlNode newXmlText(struct XmlContext* ctx, char* text, size_t n);
XmlNode newXmlInt(struct XmlContext* context, long long int value);
XmlNode newXmlUInt(struct XmlContext* context, unsigned long long int value);

/**
 * Append a new child node.
 * @return node
 */
XmlNode appendXmlNode(struct XmlContext* ctx, XmlNode node, XmlNode newChild);

/**
 * Insert a child node
 * @return node
 */
XmlNode insertXmlNode(struct XmlContext* ctx, XmlNode node, XmlNode newChild);

/**
 * Concatenate two nodes that are both the beginning of a list.
 * @param f the first node
 * @param s the second nodes
 * @return a new node
 */
XmlNode concatenateXmlNodes(struct XmlContext* context, XmlNode f, XmlNode s);

/**
 * Delete an xml node and its children
 * @param node a node
 */
void deleteXmlNode(struct XmlContext* ctx, XmlNode node);

/**
 * Print an XML node to the specified output stream.
 * @param node a node
 * @param out an output stream
 */
void printXmlNode(struct XmlContext* ctx, XmlNode node, FILE* out);


#define XML_CONTEXT 0
#define XML_TEXT(text,length) (newXmlText(XML_CONTEXT,text,length))
#define XML_ATTR(name,value) newXmlAttribute(XML_CONTEXT,name,value)
#define XML_TEXT_ATTR(name,value) (value==0 ? 0 : newXmlAttribute(XML_CONTEXT,name,XML_TEXT(value,0)))
#define XML_ELEM(name,value) (value==0 ? value : newXmlElement(XML_CONTEXT,name,value,XML_END))
#define XML_TEXT_ELEM(name,value) newXmlElement(XML_CONTEXT,name,XML_TEXT(value,0),XML_END)
#define XML_CONCAT(a,b) concatenateXmlNodes(XML_CONTEXT,(a),(b))
#define XML_APPEND(a,b) appendXmlNode(XML_CONTEXT,(a),(b))
#define XML_INSERT(a,b) insertXmlNode(XML_CONTEXT,(a),(b))
#define XML_END 0

#endif
