unit comment {

  // a simple comment
  var line4=4;

  /*
	A multiline comment
  */	
  var line9=9;

  /**
   /* A nested comment */
  var line13=13;
  */
  var line15=15;
}
