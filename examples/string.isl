unit std {
 unit strings {

  /** Use this to index a string */
  type index = integer;

  fn index_of(s:string, ch:char, start:index):index {
    if (start < 0 || start > s.length) {
       throw INVALID_INDEX : '''Index out of bonds {{start}}''';
    }
    for i : index .. s.length {
      if (s[i] == ch) {
      	 return i;
      }
    }
    return -1;
  }

  fn last_index_of(s:string, ch:char, end:index):index {
     if (end < 0 || end > s.length) {
        throw INVALID_INDEX : '''Index out of bonds {{end}}''';
     }
     
     for i : end .. 0 {
      if (s[i]==ch) {
        return i;
      }
     }
     return -1;
  }

  
 }
}
