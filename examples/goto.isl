unit Goto {

 proc check_something(x:int)
 {
   OUTER_MOST: {
     INNER_MOST: {

       break OUTER_MOST;
     }
   }
   
     
	if (x>0) {
	   goto ERROR;
	}
	// do something and return
	return;

    ERROR:
        // handle the error
        return;
 }
 

}
