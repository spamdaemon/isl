unit Math {

     type index < integer;

     generic 
     type matrix<m,n> = real[m,n];

     generic
     type column_vector<n> = real[n,1];

     generic
     type row_vector<n> = real[1,n];

     generic
     fn mul<m,n,p>(lhs:matrix<m,n>, rhs:matrix<n,p>):matrix<m,p> {

       mutable res:matrix<m,p>  = 0; // array initializer
       	    
       for (k:index =0; k<p;k=k+1) {
          for (i:index =0; i<m;i=i+1) {
	     var sum:real=0;
	     for (j:index =0;j<n;j=j+1) {
	     	 sum= sum + lhs[i,j]*rhs[j,k];
	     }
	     res[i,k] = sum;
          }
       }
       return res;
     }

     var x:integer [2,3] = [ [a,v,c], [ 1, 3, 5] ];
     var y:integer[3][2] = [ [a,v,c], [ 1, 3, 5] ];
     
}
