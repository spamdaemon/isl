unit casting {

     proc do_some_casts() {

        var x:integer = 1;
	var y:real = 1.2;
	var bits:bit[5];

	type MyBits = typeof(bits);
	 var z:integer = downcast<integer>(y);
	z = cast<integer>(x);
	bits = downcast<bits[5]>(y);
	bits = downcast<typeof(bits)>(y);
     };
}
