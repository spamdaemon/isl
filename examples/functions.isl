unit functions {

     pure fn sqr(x:real):real = x*x;
     safe fn sqrt(x:real):real;

     unsafe fn random():real;
     
}
