unit Literals {

/*
    var x:integer[3] = { 1,0,1_000_000, 0xabcdef_0123456789,0Xdeadbeef,0777,0b1000_0001 };
    var x:real[10] = { 0.0, 10., .0, 1_000_000.000_001 };
    var x_y_z:foo;
    var ch:char[12] = "123"; // cannot convert, because the constant expression is incompatible with the type
  */
    var x=1;
}
