unit Types {

     // the decimal type is different from real
     // because it can represents numbers in base 10
     // and not base 2.
     type Balance < decimal;

     var balance:Balance = decimal("123.00");
     var balance:real = 123.0;

}
