unit Templates {

     type pair < tuple {integer,real};
     
//     type range < pair<integer,integer>;

     // the type T can be derived from the arguments
     // so an explicit template argument is not required
     generic safe fn fib<T=integer|real> (x:T):T {
     	if (x<=0) { return 0; }
	if (x<=1) { return 1; }
	return fib(x-1) + fib(x-2);
     }

     // since the function does not take a parameter
     // and requires an integer value, we need the generic
     // syntax to call a template
     generic pure fn fib<x:integer> ():integer {
       if (x<=0) { return 0; }
       if (x==1) { return 1; }
       return generic:fib<(x-1)>()+generic:fib<(x-2)>();
     }

     var fib0 = generic:fib<0>();
}
