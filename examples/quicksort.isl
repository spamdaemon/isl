unit QuickSort {

     @@private
     generic fn find_pivot <T,N>(arr:T[N]):integer {
     	return 0;
     }

     @@private
     generic proc swap<T,N>(mutable arr:T[N],i:integer,j:integer) {
	var tmp=arr[i];
	arr[i]=arr[j];
	arr[j]=tmp;
     }
     
     @@private
     generic proc sort_subarray<T,N>(mutable arr:T[N], isless:isLess<T>, begin:integer, end:integer)
     {
      if (begin==end) {
	return;
      }
      
      var pivotIndex = find_pivot(arr[begin..end]);
      var pivot = arr[pivotIndex];

      mutable var i=begin;
      mutable var j=end;

      while(i!=j) {
	if (isless(arr[i],pivot)) {
	  i = i+1;
	}
	else {
	  swap(arr,i,j);
	  j=j-1;
	}
      }

      invariant j<end, j>=begin;
      
      // we've not made any progress
      if (i!=begin) {
	sort_subarray(arr,begin,i);
	sort_subarray(arr,j+1,end);
	return;
      }

      // we need to sort again; in reverse
      j=end;
      while(i!=j) {
	if (isless(pivot[j],arr[j])) {
	  j=j-1;
	}
	else {
	  swap(arr,i,j);
	  i=i+1;
	}
      }
      // a list of expression allows us to notify which of the invariants is volated
      invariant i>begin, i<=end;
      
      if (j!=end) {
 	sort_subarray(arr,begin,i-1);
	sort_subarray(arr,j,end);
      }
      
      // we're actually sorted, and all elements are same as the pivot
     }

     generic type IsLess<T> = fn(T,T):boolean;
     
     generic proc sort<T,N>(mutable arr:T[N], isless:isLess<T>) {
       sort_subarray(arr,0,arr.size,cmp);
     }

     generic fn sort<T,N>(arr:T[N], isless:isLess<T>) {
        mutable res = arr;
	sort(res,isless);
	return res;
     }

}
