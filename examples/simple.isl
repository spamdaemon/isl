unit foo {

 var x = 1;
 mutable y:integer;
 mutable str:string = "Hello, World!";
 mutable ch:char = 'X';

 /**
  * Attribute taking no arguments  can be written either as
  * @@name or @name()
  */
 @@public
 fn sqr(y:real):real {
    @ = y*y;
 }

 @@public
 @requires(x>=0)
 @provides(@ >=0);
 fn sqrt(x:real):real;

 fn sum(max:integer):real {
    var s:real=0;
    for y: 1 .. max {
    	s = s+y;
    }
    @ = s;
 }
}
