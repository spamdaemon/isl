/**
 * The unit defines the statements in the grammar
 */
unit level {

  /** Subunits */
  unit subunit {

    /** Import functionality from other units */
    import std.strings;
    

    /** Defining type aliases */
    type Message = struct {
    	 destination:string = "localhost:8080";
	 payload:octet[];  // an unbounded array
    };

    /** A define a subtype of message */
    type HeartbeatMessage < Message;

    /** A shutdown message */
    type ShutdownMessage < Message;

    /** a function that takes an unbounded array and returns a unbounded array */
    fn reverse(m:octet[]):octet[] {
       // variables can be mutable
       mutable res:octet[] = new octet[m.length](0);

       /** traditional for-loop with initializer and increment block */
       for (i:integer = 1;i <= m.length; i=i+1) {
       	   res[m.length-i] = m[i-1];
       }
       return res;
    }

    /** A procedure has no return value */
    proc send(message:Message) {
      // text interpolation
      print('''Sending message to "{{message.destination}}"''');
    }

    /** function/procedure overloading */    
    proc send(message:Message[]) {
    	 // FOR EACH loops
	 for m : message {
	     send(m);
	 }
    }

    fn blocking_receive(s:socket):Message {
       mutable received:opt Message;
       // DO-UNTIL loops
       do {
          // basic error handling
       	  try {
	     received = wait_for_message(s,100);
	  }
	  catch {
	    // we don't care about the actual error here, just that we can easily recover
	    
	    // nothing to do
	    print("No message received; waiting some more");
	  }
       } until (received);
       return *received;
    }

    // function defined using a simple expression
    fn is_ready(s:socket):boolean = true;

    proc blocking_send(s:socket,m:Message) {
       mutable sent:boolean = false;
       until (sent) {
       	  // first argument of a function/procedure can be used like an object
	  sent = s.is_ready(); // equivalent to is_ready(s)
          // IF statement
          if (sent) {
	    send(m);
	    sent=true;
	  }
       }
    }

    proc send(msg:ShutdownMessage) {
      // type casting (implicit)
      var m : Message = msg;
      send(m);
    }

    // support for goto statements allows
    // language to be used as  target language
    // of another compiler
    proc goto_statements()
    {
	var m:Message = new Message();
		
	if (!send_message(m)) {
	   goto SEND_FAILED;
	}
	return;
      SEND_FAILED:
        print("Failed to send message");
    }
    // support for invariants
    fn increment(i:integer):integer {
       var j:integer = i+1;
       // invaraints take a list of expressions, each of which must be true
       invariant j>i, i<j;
       return j;
    }
    // switch statements
    fn switch_on(i:integer):boolean {
       mutable res:boolean;
       switch(i) {
        // each case can take a list of expressions
	// there is no fall-through
        -1,1: res= true;
	 // a default cast applies when no other case applies
	default: res= false;
       }
    }
    // variable aliases are pointer/reference like objects
    proc aliases(argi:integer)
    {
	mutable i:integer = argi;
	
        // array with 10 elements initialized to 1 each
	mutable arr:integer[] = new integer[10](1);

 	// j always aliases i and has reference semantics, and not copy semantics
	alias j=i;
	var jj=i;

	invariant i==j,jj==i;
	i=i+1;
	invariant i==j,jj!=i;
    }
   // macros
    proc test_macro()
    {
	var i:integer =0;
	mutable arr:integer[] = new integer[1](0);
	// the right hand side of a macro replaces any occurrence of the left-hand side
	// the scope of the variables on the right-hand side will always be the scope
	// at which the macro was defined
	macro x = arr[i];
	{
	  mutable i:integer = 0; // shadowing the enclosing i variable
   	  while(i<10) {
	    // this actually always sets the same slot in the array
	    // because the i in the x macro refers the i in the enclosing scope.
	    x = i;   
	    i=i+1;
	  }
	}
    }
    
  }

}
