unit std {

  type Hash < bit[32];

  fn hash(str:string):Hash {
     return 0;         
  }
}

unit Person {

  type Person < struct {
     firstName : string;
     middleInitial : opt char;
     lastName : string;
     age : integer;
  };

  /**
   * Print the name and age of the specified person.
   * @param p a person
   */
  proc print(p:Person)  {
       if (0 < p.age < 100) {
       }
       else {
       }
  }

 /** Generate a hash for a person */
 fn hash(p:Person):Hash { 
    return std.hash(p.firstName) ^ std.hash(p.middleInitial) ^ std.hash(p.lastName);
 }
}
