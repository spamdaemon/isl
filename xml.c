#include "xml.h"
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

enum XmlNodeType {
		  XML_TEXT_NODE = 1,
		  XML_ATTRIBUTE_NODE = 2,
		  XML_ELEMENT_NODE = 4
};

struct XmlNodeImpl {
  int deleted;
  int type;
  size_t index;
  char* name;
  // the next node in the list
  struct XmlNodeImpl* next;
  // the children
  struct XmlNodeImpl* children;
  // the text value
  char* text;
};

static void destroyXmlNode(struct XmlNodeImpl* node)
{
  if (node->deleted == 0xdeadbeef) {
    fprintf(stderr, "ERROR: node already deleted");
    abort();
  }
  node->deleted = 0xdeadbeef;
  if (node->name) {
    free(node->name);
    node->name = 0;
  }
  if (node->text) {
    free(node->text);
    node->text = 0;
  }
  struct XmlNodeImpl* c = node->children;
  while (c) {
    struct XmlNodeImpl* n = c->next;
    destroyXmlNode(c);
    c = n;
  }
  free(node);
}

#define MAX_XML_CONTEXT_NODES 1000000

struct XmlContext {
  struct XmlNodeImpl* nodes[MAX_XML_CONTEXT_NODES];
};

static struct XmlContext CONTEXT;


static struct XmlNodeImpl* getNode(struct XmlContext* context, XmlNode index)
{
  if (index >= MAX_XML_CONTEXT_NODES) {
    fprintf(stderr,"Invalid index specified\n");
    abort();
  }
  
  return context->nodes[index];
}

struct XmlContext* createXmlContext()
{
  struct XmlContext* ctx = (struct XmlContext*) calloc(1,sizeof(struct XmlContext));
  return ctx;
}
struct XmlContext* getContext(struct XmlContext* ctx)
{
	if (ctx) {
		return ctx;
	}
	else {
		return &CONTEXT;
	}
}

void destroyXmlContext(struct XmlContext* context)
{
  if (context) {
    for (size_t i=1;i<MAX_XML_CONTEXT_NODES;++i) {
      if (context->nodes[i]) {
	deleteXmlNode(context,i);
	context->nodes[i]=0;
      }
    }
    free(context);
  }
}

static struct XmlNodeImpl* makeXmlNode(struct XmlContext* context, int type, const char* name) {
  struct XmlContext* ctx = getContext(context);
  size_t index=0;
  for(index=1;index<MAX_XML_CONTEXT_NODES;++index) {
    if (!ctx->nodes[index]) {
      break;
    }
  }
  
  if (index == MAX_XML_CONTEXT_NODES) {
    fprintf(stderr,"Too many unused nodes\n");
    abort();
  }
  
  struct XmlNodeImpl* node = (struct XmlNodeImpl*) malloc(sizeof(struct XmlNodeImpl));
  node->deleted = 0;
  node->type = type;
  if (name) {
    node->name = strdup(name);
  } else {
    node->name = 0;
  }
  node->next=0;
  node->children = 0;
  node->text = 0;
  node->index = index;
  ctx->nodes[index] = node;
  return node;
}

static struct XmlNodeImpl* concatenateNodes(struct XmlContext* context, struct XmlNodeImpl* f, struct XmlNodeImpl* s)
{
  struct XmlContext* ctx = getContext(context);
  if (!f) {
    return s;
  }
  if (!s) {
    return f;
  }
  struct XmlNodeImpl* last = f;
  while(last->next) {
    last=last->next;
  }
  last->next = s;
  if (ctx->nodes[s->index] == s) {
    ctx->nodes[s->index] = 0;
    s->index=0;
  }
  
  return f;
}

XmlNode renameXmlNode(struct XmlContext* context, const char* name, XmlNode node)
{
	  struct XmlContext* ctx = getContext(context);
  struct XmlNodeImpl* ptr = getNode(ctx,node);
  if (!ptr) {
    fprintf(stderr,"Node not found\n");
    return node;
  }
  if (ptr->type==XML_TEXT_NODE) {
    return node;
  }
  free(ptr->name);
  ptr->name = strdup(name);
  return node;
}

XmlNode insertXmlNode(struct XmlContext* context, XmlNode node, XmlNode newChild)
{
	  struct XmlContext* ctx = getContext(context);
  struct XmlNodeImpl* ptr = getNode(ctx,node);
  if (!ptr) {
    fprintf(stderr,"Node not found\n");
    return node;
  }
  ptr->children = concatenateNodes(ctx,getNode(ctx,newChild),ptr->children);
  return node;
}

XmlNode appendXmlNode(struct XmlContext* context, XmlNode node, XmlNode newChild)
{
	  struct XmlContext* ctx = getContext(context);
  struct XmlNodeImpl* ptr = getNode(ctx,node);
  if (!ptr) {
    fprintf(stderr,"Node not found\n");
    return node;
  }
  
  struct XmlNodeImpl* cptr = getNode(ctx,newChild);
  if (!cptr) {
    fprintf(stderr,"Child not found\n");
    return node;
  }
  ptr->children = concatenateNodes(ctx,ptr->children,getNode(ctx,newChild));
  return node;
}

XmlNode newXmlElement(struct XmlContext* context, const char* name,  ...) {
	  struct XmlContext* ctx = getContext(context);
  struct XmlNodeImpl* node = makeXmlNode(ctx,XML_ELEMENT_NODE,name);

  va_list args;
  va_start(args, name);
  for (XmlNode n; (n=va_arg(args,XmlNode))!=0;) {
    struct XmlNodeImpl* ptr = getNode(ctx,n);
    node->children = concatenateNodes(ctx,node->children,ptr);
  }
  va_end(args);
  return node->index;
}

XmlNode newXmlAttribute(struct XmlContext* context, const char* name, XmlNode text) {
	  struct XmlContext* ctx = getContext(context);
  struct XmlNodeImpl* ptr = getNode(ctx,text);
  if (!ptr) {
    fprintf(stderr,"Node not found\n");
    return text;
  }
  if (ptr->type!=XML_TEXT_NODE) {
    fprintf(stderr,"Attribute allows only a single text node as child");
    return text;
  }
  struct XmlNodeImpl* node = makeXmlNode(ctx,XML_ATTRIBUTE_NODE,name);
  node->children=ptr;
  ctx->nodes[ptr->index]=0;
  ptr->index=0;
  return node->index;
}

XmlNode newXmlText(struct XmlContext* context, char* text, size_t n)
{
	  struct XmlContext* ctx = getContext(context);
  struct XmlNodeImpl* node = makeXmlNode(ctx,XML_TEXT_NODE,0);
  if (n == 0) {
    node->text = strdup(text);
  } else {
    node->text = strndup(text, n);
  }
  return node->index;
}

XmlNode newXmlInt(struct XmlContext* context, long long int value)
{
	char tmp[100];
	snprintf(tmp,sizeof(tmp)-1,"%lld",value);
	return newXmlText(context,tmp,0);
}

XmlNode newXmlUInt(struct XmlContext* context, unsigned long long int value)
{
	char tmp[100];
	snprintf(tmp,sizeof(tmp)-1,"%llu",value);
	return newXmlText(context,tmp,0);
}

XmlNode concatenateXmlNodes(struct XmlContext* context, XmlNode f, XmlNode s)
{
	  struct XmlContext* ctx = getContext(context);
  struct XmlNodeImpl* node = concatenateNodes(ctx,getNode(ctx,f),getNode(ctx,s));
  if (node) {
    return node->index;
  }
  return 0;
}

void deleteXmlNode(struct XmlContext* context, XmlNode index)
{
	  struct XmlContext* ctx = getContext(context);
  struct XmlNodeImpl* node = getNode(ctx,index);
  if (node) {
    destroyXmlNode(node);
    ctx->nodes[index]==0;
  }
}

static void printText(char* text, FILE* out)
{
  while(*text != '\0') {
    switch(*text) {
    case '<':
      fprintf(out,"&lt;");break;
    case '>':
      fprintf(out,"&gt;");break;
    case '"':
      fprintf(out,"&quot;");break;
    case '\'':
      fprintf(out,"&apos;");break;
    case '&':
      fprintf(out,"&amp;");break;
    default:
	fprintf(out,"%c",*text);
    }
    ++text;
  }
}

static void printIndent(size_t indent, FILE* out)
{
  size_t i=0;
  for (i=0;i<indent;++i) {
    fprintf(out,"  ");
  }
}
static void printTextNode(struct XmlNodeImpl* node, FILE* out)
{
  if (node->text) {
    printText(node->text,out);
  }
}

static void printIndentedXmlNode(struct XmlNodeImpl* node, size_t indent, FILE* out)
{
  if (node->type==XML_TEXT_NODE) {
    printTextNode(node,out);
    return;
  }
  if (node->type==XML_ATTRIBUTE_NODE) {
    fprintf(out," %s=\"",node->name);
    for (struct XmlNodeImpl* c = node->children;c;c=c->next) {
      printTextNode(c,out);
    }
    fprintf(out,"\"");
    return;
  }
  if (node->type==XML_ELEMENT_NODE) {
    printIndent(indent,out);
    fprintf(out,"<%s",node->name);
    // print attributes
    int child_types = 0;
    for (struct XmlNodeImpl* c = node->children;c;c=c->next) {
      child_types |= c->type;
      if (c->type==XML_ATTRIBUTE_NODE) {
	printIndentedXmlNode(c,0,out);
      }
    }
    if (child_types & ~XML_ATTRIBUTE_NODE) { // if we have element children
      fprintf(out,">");
      int printNL=0;
      for (struct XmlNodeImpl* c = node->children;c;c=c->next) {
	if (c->type==XML_ELEMENT_NODE) {
	  fprintf(out,"\n");
	  printIndentedXmlNode(c,1+indent,out);
	  printNL=1;
	}
	else if (c->type==XML_TEXT_NODE) {
	  printTextNode(c,out);
	  printNL=0;
	}
      }
      if (printNL) {
	fprintf(out,"\n");
	printIndent(indent,out);
      }
      fprintf(out,"</%s>",node->name);
    }
    else {
      fprintf(out,"/>");
    }
  }
}

void printXmlNode(struct XmlContext* context, XmlNode node, FILE* out)
{
  struct XmlContext* ctx = getContext(context);
  struct XmlNodeImpl* tmp = getNode(ctx,node);
  if (tmp) {
	fprintf(out,"<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
    printIndentedXmlNode(tmp,0,out);
    fprintf(out,"\n");
  }
  else {
    fprintf(stderr,"No such node to print\n");
  }
}

