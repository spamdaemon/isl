#include "ast.h"
#include "grammar.tab.h"

int main(int argc, char** argv)
{
  int ok=1;
  AST=0;
  switch(yyparse()) {
  case 0:
    printXmlNode(XML_CONTEXT,AST,stdout);
    ok=0;
    break;
  case 1:
    fprintf(stderr,"Syntax error");
    break;
  case 2:
    fprintf(stderr,"Out of memory");
    break;
  default:
    fprintf(stderr,"Unexpected error");
    break;
  }
  return ok;
}
