
all: main

clean:
	rm -f *.o grammar.tab.* grammar.output;
	find -maxdepth 1 -executable \! -type d | xargs rm -f;

%.o: %.c grammar.tab.h
	gcc -g -c -o "$@" "$<";

%.c: %.l
	flex  -o "$@" "$<";

grammar.tab.c: grammar.y
	bison --locations -v -Wconflicts-sr -Wconflicts-rr -d "$<"

grammar.tab.h: grammar.y
	bison --locations -v -Wconflicts-sr -Wconflicts-rr -d "$<"

main: main.o ast.o xml.o bison.o lexer.o grammar.tab.o
	gcc -o "$@" $^;
	./main < examples/features.isl > features.xml

