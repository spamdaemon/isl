%{
#include "ast.h"
#include "bison.h"
%}
// Turn on location tracking
%locations
// All tokens and produces produce a node or a nullpointer
%define api.value.type { XmlNode }

// reserved words
%token LITERAL_BOOLEAN LITERAL_OCTET LITERAL_BIT LITERAL_INTEGER LITERAL_REAL LITERAL_STRING LITERAL_CHARACTER
%token BEGIN_TEXT END_TEXT BEGIN_EXPR END_EXPR
%token IDENTIFIER STD
%token VAR MUTABLE PROCEDURE FUNCTION RETURN RESULT
%token IF ELSE
%token SWITCH DEFAULT
%token FOR WHILE UNTIL DO BREAK CONTINUE GOTO
%token ERROR TRY CATCH
%token TYPE
%token TYPEOF STRUCT ENUM UNION TUPLE STREAM LIST OPTIONAL
%token TYPE_CHAR TYPE_INTEGER TYPE_BOOLEAN TYPE_REAL TYPE_DECIMAL TYPE_STRING TYPE_BIT TYPE_OCTET
%token NEW
%token UNIT IMPORT 
%token CLASS
%token ALIAS MACRO
%token SAFE PURE UNSAFE GENERIC
%token INVARIANT 
// safe cast are able to perform loss-less conversion between two types or cause a runtime error
// an unsafe cast will not cause a runtime error, but may result in loss of information
%token CAST LOSSY_CAST
%token ARG_ATTRIBUTE NO_ARG_ATTRIBUTE
/* TOKENS */
%token MINUS_OPERATOR PLUS_OPERATOR
%token LE_OPERATOR LT_OPERATOR EQ_OPERATOR NE_OPERATOR GT_OPERATOR GE_OPERATOR
%token LOGICAL_AND_OPERATOR LOGICAL_OR_OPERATOR LOGICAL_XOR_OPERATOR LOGICAL_NOT_OPERATOR
%token BITWISE_AND_OPERATOR BITWISE_OR_OPERATOR BITWISE_XOR_OPERATOR BITWISE_NOT_OPERATOR
%token SHIFT_RIGHT_OPERATOR ARITHMETIC_SHIFT_RIGHT_OPERATOR SHIFT_LEFT_OPERATOR
%token MUL_OPERATOR DIV_OPERATOR MOD_OPERATOR
%token UNARY_TERM_OPERATOR
%token MAP_OPERATOR 
%token ASSIGN
%token RANGE
%%

// THE input file must contain at least 1 unit
Root: List_Statement  
 { $$=newXmlElement(XML_CONTEXT,"ast:statements",$1,
 	XML_TEXT_ATTR("xmlns:ast","http://isl/ast"),
 	XML_TEXT_ATTR("xmlns:type","http://isl/types"),
 	XML_TEXT_ATTR("xmlns:expr","http://isl/expressions"),
 	XML_TEXT_ATTR("xmlns:stmt","http://isl/statements"),
 	XML_TEXT_ATTR("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance"),
 	XML_TEXT_ATTR("xsi:schemaLocation","http://isl/ast ast/ast.xsd http://isl/types ast/types.xsd http://isl/expressions ast/expressions.xsd http://isl/statements ast/statements.xsd"),
 	XML_END); SOURCE(($$),(@$)); AST=$$; }

////////////////////////////////////////////////////////////////////////////
// Names
////////////////////////////////////////////////////////////////////////////
Qname: List_Identifier  
 { $$=newXmlElement(XML_CONTEXT,"ast:qname",XML_ELEM("ast:path",$1),XML_END); SOURCE(($$),(@$));  }

Identifier: IDENTIFIER  
 { $$=newXmlElement(XML_CONTEXT,"ast:identifier",XML_ELEM("ast:value",$1),XML_END); SOURCE(($$),(@$));  }
| STD   
 { $$=newXmlElement(XML_CONTEXT,"ast:identifier",XML_ELEM("ast:value",$1),XML_END); SOURCE(($$),(@$));  }

Label: IDENTIFIER
 { $$=newXmlElement(XML_CONTEXT,"stmt:label",XML_ELEM("ast:value",$1),XML_END); SOURCE(($$),(@$));  }


		    
////////////////////////////////////////////////////////////////////////////
// STATEMENTS
////////////////////////////////////////////////////////////////////////////

Statement: StatementBlock |
OptList_Attribute AttributableStatement  { $$=insertXmlNode(XML_CONTEXT,$2,XML_ELEM("ast:attributes",$1)); }




AttributableStatement: NoStatement ';'  { $$=$1; }
| UnitStatement  { $$=$1; }
| ImportStatement ';'  { $$=$1; }
| VarStatement ';'  { $$=$1; }
| Alias ';'  { $$=$1; }
| LabelStatement
| BreakStatement ';'  { $$=$1; }
| ContinueStatement ';'  { $$=$1; }
| ReturnStatement ';'  { $$=$1; }
| GotoStatement ';'  { $$=$1; }
| RaiseError ';'  { $$=$1; }
| IfStatement
| SwitchStatement
| WhileLoop
| UntilLoop
| DoWhileLoop ';'  { $$=$1; }
| DoUntilLoop ';'  { $$=$1; }
| ForEachLoop
| ForLoop
| Assignment ';'  { $$=$1; }
| Invariant ';'   { $$=$1; }
| TryRecover
| MacroDeclaration
| FunctionDeclaration
| ProcedureDeclaration
| ProcedureCallStatement ';'  { $$=$1; }
| TypeDeclaration
;

LocalStatementBlock : '{' OptList_Statement '}'  { $$=$2; }

StatementBlock : LocalStatementBlock { $$=newXmlElement(XML_CONTEXT,"stmt:statement-block",$1,XML_END); SOURCE(($$),(@$));  }

SimpleStatement :  ProcedureCallStatement | Assignment

NoStatement:  
 { $$ = newXmlElement(XML_CONTEXT,"stmt:nostatement",XML_END); SOURCE(($$),(@$));  }

UnitStatement: UNIT Qname LocalStatementBlock  
 { $$ = newXmlElement(XML_CONTEXT,"stmt:unit",$2,XML_ELEM("stmt:statements",$3),XML_END); SOURCE(($$),(@$));  }

ImportStatement: IMPORT Qname  
 { $$=newXmlElement(XML_CONTEXT,"stmt:import",$2,XML_END); SOURCE(($$),(@$));  }

VarStatement: VarKind Identifier Opt_VariableType Opt_Initializer 
 { $$=newXmlElement(XML_CONTEXT,"stmt:var",$2,XML_ELEM("stmt:kind",$1),XML_ELEM("stmt:type",$3),XML_ELEM("stmt:initializer",$4),XML_END); SOURCE(($$),(@$));  }

Alias: ALIAS Identifier Opt_VariableType ASSIGN Expression                
 { $$=newXmlElement(XML_CONTEXT,"stmt:alias",$2,XML_ELEM("stmt:type",$3),XML_ELEM("stmt:expression",$5),XML_END); SOURCE(($$),(@$));  }

LabelStatement: Identifier ':' 
 { $$=newXmlElement(XML_CONTEXT,"stmt:declare-label",$1,XML_END); SOURCE(($$),(@$));  }

BreakStatement: BREAK Opt_Label 
 { $$=newXmlElement(XML_CONTEXT,"stmt:break",$2,XML_END); SOURCE(($$),(@$));  }

ContinueStatement: CONTINUE Opt_Label 
 { $$=newXmlElement(XML_CONTEXT,"stmt:continue",$2,XML_END); SOURCE(($$),(@$));  }

GotoStatement: GOTO Label 
 { $$=newXmlElement(XML_CONTEXT,"stmt:goto",$2,XML_END); SOURCE(($$),(@$));  }

ReturnStatement: RETURN Opt_Expression 
 { $$=newXmlElement(XML_CONTEXT,"stmt:yield",XML_ELEM("stmt:result",$2),XML_END); SOURCE(($$),(@$));  }

RaiseError: ERROR Expression 
 { $$=newXmlElement(XML_CONTEXT,"stmt:raise",XML_ELEM("stmt:value",$2),XML_END); SOURCE(($$),(@$));  }

IfStatement: IF '(' Expression ')' StatementBlock Opt_Else 
 { $$ = newXmlElement(XML_CONTEXT,"stmt:if",XML_ELEM("stmt:condition",$3),XML_ELEM("stmt:iftrue",$5),XML_ELEM("stmt:iffalse",$6),XML_END); SOURCE(($$),(@$));  }
Else: ELSE StatementBlock 
 { $$=$2; }

SwitchStatement: SWITCH '(' Expression ')' '{' List_SwitchCase '}' 
 { $$ = newXmlElement(XML_CONTEXT,"stmt:switch",XML_ELEM("stmt:on",$3),XML_ELEM("stmt:cases",$6),XML_END); SOURCE(($$),(@$));  }

WhileLoop: WHILE '(' Expression ')' StatementBlock 
 { $$=newXmlElement(XML_CONTEXT,"stmt:while",XML_ELEM("stmt:condition",$3),XML_ELEM("stmt:do",$5),XML_END); SOURCE(($$),(@$));  }

UntilLoop: UNTIL '(' Expression ')' StatementBlock 
 { $$=newXmlElement(XML_CONTEXT,"stmt:until",XML_ELEM("stmt:condition",$3),XML_ELEM("stmt:do",$5),XML_END); SOURCE(($$),(@$));  }

DoWhileLoop: DO StatementBlock WHILE '(' Expression ')' 
 { $$=newXmlElement(XML_CONTEXT,"stmt:do-while",XML_ELEM("stmt:condition", $5),XML_ELEM("stmt:do",$2),XML_END); SOURCE(($$),(@$));  }

DoUntilLoop: DO StatementBlock UNTIL '(' Expression ')' 
 { $$=newXmlElement(XML_CONTEXT,"stmt:do-until",XML_ELEM("stmt:condition", $5),XML_ELEM("stmt:do",$2),XML_END); SOURCE(($$),(@$));  }

ForEachLoop: FOR Identifier  ':' Expression  StatementBlock 
 { $$ = newXmlElement(XML_CONTEXT,"stmt:for-each",XML_ELEM("stmt:var",$2), XML_ELEM("stmt:source", $4),XML_ELEM("stmt:do", $5),XML_END); SOURCE(($$),(@$));  }

ForLoop:     FOR '(' OptList_Variable ';' Expression ';' OptList_SimpleStatement ')' StatementBlock  
 { $$ = newXmlElement(XML_CONTEXT,"stmt:for",XML_ELEM("stmt:condition",$5),XML_ELEM("stmt:do",$9),XML_ELEM("stmt:preLoop",$3),XML_ELEM("stmt:postBody",$7),XML_END); SOURCE(($$),(@$));  }

// the various types of statements
Assignment: AssignableExpression ASSIGN Expression
 { $$=newXmlElement(XML_CONTEXT,"stmt:assign",XML_ELEM("stmt:destination",$1),XML_ELEM("stmt:source",$3),XML_END); SOURCE(($$),(@$));  }

Invariant: INVARIANT List_Expression Opt_Details 
 { $$=newXmlElement(XML_CONTEXT,"stmt:invariant",XML_ELEM("stmt:expressions",$2),XML_ELEM("stmt:message",$3),XML_END); SOURCE(($$),(@$));  }

TryRecover: TRY StatementBlock CATCH StatementBlock 
 { $$=newXmlElement(XML_CONTEXT,"stmt:try-recover",XML_ELEM("stmt:try",$2),XML_ELEM("stmt:recover",$4),XML_END); SOURCE(($$),(@$));  }


MacroDeclaration: MACRO Identifier Opt_MacroParameters ASSIGN Expression ';'  
 { $$=newXmlElement(XML_CONTEXT,"stmt:macro",$2,newXmlElement(XML_CONTEXT,"stmt:parameters",$3,XML_END),XML_ELEM("stmt:replacement",$5),XML_END); SOURCE(($$),(@$));  }
MacroParameters: '(' List_Identifier ')' 
 { $$=$2;  SOURCE(($$),(@$)); }


FunctionDeclaration:
Opt_Purity FUNCTION Identifier '(' OptList_Parameter ')' Opt_VariableType FunctionBody 
{ $$=newXmlElement(XML_CONTEXT,"stmt:function",$3,
		   newXmlElement(XML_CONTEXT,"stmt:signature",XML_ELEM("type:parameters",$5),XML_ELEM("type:result",$7),XML_ELEM("type:purity",$1),XML_END),
		   XML_ELEM("stmt:body",$8),XML_END);
  SOURCE(($$),(@$));
}
| Opt_Purity FUNCTION Identifier '(' OptList_Parameter ')' VariableType ';' 
{ $$=newXmlElement(XML_CONTEXT,"stmt:function",$3,
		   newXmlElement(XML_CONTEXT,"stmt:signature",XML_ELEM("type:parameters",$5),XML_ELEM("type:result",$7),XML_ELEM("type:purity",$1),XML_END),XML_END);
  SOURCE(($$),(@$));
}
|GENERIC Opt_Purity FUNCTION Identifier TemplateParameters '(' OptList_Parameter ')' Opt_VariableType FunctionBody 
{ $$=newXmlElement(XML_CONTEXT,"stmt:function",$4,
		   newXmlElement(XML_CONTEXT,"stmt:signature",XML_ELEM("type:template-parameters",$5),XML_ELEM("type:parameters",$7),XML_ELEM("type:result",$9),XML_ELEM("type:purity",$2),XML_END),
		   XML_ELEM("stmt:body",$10),XML_END);
  SOURCE(($$),(@$));
}
| GENERIC Opt_Purity FUNCTION Identifier TemplateParameters  '(' OptList_Parameter ')' VariableType ';' 
{ $$=newXmlElement(XML_CONTEXT,"stmt:function",$4,
		   newXmlElement(XML_CONTEXT,"stmt:signature",XML_ELEM("type:template-parameters",$5),XML_ELEM("type:parameters",$7),XML_ELEM("type:result",$9),XML_ELEM("type:purity",$2),XML_END),XML_END);
  SOURCE(($$),(@$));
}


ProcedureDeclaration:
PROCEDURE Identifier '(' OptList_Parameter ')' FunctionBody 
{ $$=newXmlElement(XML_CONTEXT,"stmt:procedure",$2,
		   newXmlElement(XML_CONTEXT,"stmt:signature",XML_ELEM("type:parameters",$4),XML_END),
		   XML_ELEM("stmt:body",$6),XML_END);
  SOURCE(($$),(@$));
}
|PROCEDURE Identifier '(' OptList_Parameter ')' ';' 
{ $$=newXmlElement(XML_CONTEXT,"stmt:procedure",$2,
		   newXmlElement(XML_CONTEXT,"stmt:signature",XML_ELEM("type:parameters",$4),XML_END),XML_END);
  SOURCE(($$),(@$));
}
|GENERIC PROCEDURE Identifier TemplateParameters '(' OptList_Parameter ')' FunctionBody 
{ $$=newXmlElement(XML_CONTEXT,"stmt:procedure",$3,
		   newXmlElement(XML_CONTEXT,"stmt:signature",XML_ELEM("type:template-parameters",$4),XML_ELEM("type:parameters",$6),XML_END),
		   XML_ELEM("stmt:body",$8),XML_END);
  SOURCE(($$),(@$));
}
| GENERIC PROCEDURE Identifier TemplateParameters '(' OptList_Parameter ')' ';' 
{ $$=newXmlElement(XML_CONTEXT,"stmt:procedure",$3,
		   newXmlElement(XML_CONTEXT,"stmt:signature",XML_ELEM("type:template-parameters",$4),XML_ELEM("type:parameters",$6),XML_END),
		   XML_END);
  SOURCE(($$),(@$));
}


ProcedureCallStatement: AssignableExpression ArgumentList
 { $$ = newXmlElement(XML_CONTEXT,"stmt:call",XML_ELEM("stmt:callee",$1),$2,XML_END); SOURCE(($$),(@$));  }

TypeDeclaration:
 TYPE Identifier ';' 
 { $$=newXmlElement(XML_CONTEXT,"stmt:type",$2,XML_END); SOURCE(($$),(@$));  }
| TYPE Identifier LT_OPERATOR TypeDefinition ';' 
 { $$=newXmlElement(XML_CONTEXT,"stmt:subtype",$2,XML_ELEM("type:type-alias",$4),XML_END); SOURCE(($$),(@$));  }
| TYPE Identifier ASSIGN TypeDefinition ';'  
 { $$=newXmlElement(XML_CONTEXT,"stmt:typedef",$2,XML_ELEM("type:subtype",$4),XML_END); SOURCE(($$),(@$));  }
| GENERIC TYPE Identifier TemplateParameters ';' 
 { $$=newXmlElement(XML_CONTEXT,"stmt:type",$3,XML_ELEM("stmt:generic",$4),XML_END); SOURCE(($$),(@$));  }
| GENERIC TYPE Identifier TemplateParameters LT_OPERATOR TypeDefinition ';' 
 { $$=newXmlElement(XML_CONTEXT,"stmt:subtype",$3,XML_ELEM("stmt:generic",$4),XML_ELEM("stmt:basetype",$6),XML_END); SOURCE(($$),(@$));  }
| GENERIC TYPE Identifier TemplateParameters ASSIGN TypeDefinition ';'  
 { $$=newXmlElement(XML_CONTEXT,"stmt:typealias",$3,XML_ELEM("stmt:generic",$4),XML_ELEM("stmt:type",$6),XML_END); SOURCE(($$),(@$));  }


Attribute: NO_ARG_ATTRIBUTE 
 { $$=newXmlElement(XML_CONTEXT,"attribute",$1,XML_END); SOURCE(($$),(@$));  }
| ARG_ATTRIBUTE ArgumentList
 { $$=newXmlElement(XML_CONTEXT,"attribute",$1,$2,XML_END); SOURCE(($$),(@$));  }

Details: ':' Expression 
 { $$=$2; }

SwitchCase: DEFAULT ':' Statement  
 { $$=newXmlElement(XML_CONTEXT,"stmt:case",newXmlElement(XML_CONTEXT,"stmt:ifmatch",XML_END),XML_ELEM("stmt:do",$3),XML_END); SOURCE(($$),(@$));  }
| List_Expression ':' Statement 
 { $$=newXmlElement(XML_CONTEXT,"stmt:case",XML_ELEM("stmt:ifmatch",$1),XML_ELEM("stmt:do",$3),XML_END); SOURCE(($$),(@$));  }

Parameter:  Identifier Opt_VariableType 
 { $$=newXmlElement(XML_CONTEXT,"type:parameter",$1,XML_ELEM("ast:type",$2),XML_END); SOURCE(($$),(@$));  }

FunctionBody: ASSIGN Expression ';' 
 { $$=$2; }
| StatementBlock

Variable: Identifier Opt_VariableType Initializer 
 { $$=newXmlElement(XML_CONTEXT,"variable",$1,XML_ELEM("stmt:type",$2),XML_ELEM("stmt:initializer",$3),XML_END); SOURCE(($$),(@$));  }

VarKind : VAR 
 { $$=0; }
| Mutable 
 { $$=$1; }
| Mutable VAR 
 { $$=$1; }

VariableType: ':' Type 
 { $$=$2; }

Initializer: ASSIGN Expression 
 { $$=$2; }
| ASSIGN '{' OptList_Expression '}' 
 { $$ = newXmlElement(XML_CONTEXT,"initializer",XML_ELEM("stmt:values",$3),XML_END); SOURCE(($$),(@$));  }

TemplateParameters: LT_OPERATOR List_TemplateParameter GT_OPERATOR 
 { $$=$2; }

TemplateParameter: Identifier        
 { $$=newXmlElement(XML_CONTEXT,"template-parameter",$1,XML_END); SOURCE(($$),(@$));  }
| Identifier ASSIGN AltList_Type     
 { $$=newXmlElement(XML_CONTEXT,"template-parameter",$1,XML_ELEM("stmt:match",$3),XML_END); SOURCE(($$),(@$));  }
| Identifier ASSIGN AltList_Literal  
 { $$=newXmlElement(XML_CONTEXT,"template-parameter",$1,XML_ELEM("stmt:match",$3),XML_END); SOURCE(($$),(@$));  }
| Identifier ':' Type                
 { $$=newXmlElement(XML_CONTEXT,"template-parameter",$1,XML_ELEM("stmt:type",$3),XML_END); SOURCE(($$),(@$));  }


Mutable: MUTABLE 
{ $$=XML_TEXT("mutable",0); } 

////////////////////////////////////////////////////////////////////////////
// TYPES
////////////////////////////////////////////////////////////////////////////

TypeDefinition: Type|StructType|UnionType|EnumType|TupleType|FunctionType|ProcedureType|RangeType;

StructType: STRUCT '{' List_Member '}' 
 { $$ = newXmlElement(XML_CONTEXT,"type:struct",$3,XML_END); SOURCE(($$),(@$));  }

UnionType: UNION '{' List_Member '}' 
 { $$ = newXmlElement(XML_CONTEXT,"type:union",$3,XML_END); SOURCE(($$),(@$));  }

EnumType: ENUM '{' List_Identifier '}' 
 { $$ = newXmlElement(XML_CONTEXT,"type:enum",XML_ELEM("type:enumeration",$3),XML_END); SOURCE(($$),(@$));  }

TupleType: TUPLE '{' OptList_Type '}' 
 { $$ = newXmlElement(XML_CONTEXT,"type:tuple",XML_ELEM("type:types",$3),XML_END); SOURCE(($$),(@$));  }

RangeType: Literal RANGE Literal 
 { $$ = newXmlElement(XML_CONTEXT,"type:range",XML_ELEM("typemin",$1),XML_ELEM("type:max",$3),XML_END); SOURCE(($$),(@$));  }

FunctionType: Opt_Purity FUNCTION '(' OptList_Type ')' VariableType 
 { $$ = newXmlElement(XML_CONTEXT,"type:function",XML_ELEM("type:purity",$1),XML_ELEM("ast:arguments",$4),XML_ELEM("ast:type",$6),XML_END); SOURCE(($$),(@$));  }

ProcedureType: Opt_Purity PROCEDURE '(' OptList_Type ')' 
 { $$ = newXmlElement(XML_CONTEXT,"type:procedure",XML_ELEM("type:purity",$1),XML_ELEM("ast:arguments",$4),XML_END); SOURCE(($$),(@$));  }

StreamType: STREAM LT_OPERATOR ArrayType GT_OPERATOR 
 { $$=newXmlElement(XML_CONTEXT,"type:stream",XML_ELEM("ast:type",$3),XML_END); SOURCE(($$),(@$));  }

ListType: LIST LT_OPERATOR ArrayType GT_OPERATOR 
 { $$=newXmlElement(XML_CONTEXT,"type:list",XML_ELEM("ast:type",$3),XML_END); SOURCE(($$),(@$));  }

ArrayType: OptionalType | OptionalType List_ArrayBound 
 { $$=newXmlElement(XML_CONTEXT,"type:array",XML_ELEM("ast:type",$1),XML_ELEM("ast:bounds",$2),XML_END); SOURCE(($$),(@$));  }

OptionalType: Typename | OPTIONAL Typename 
 { $$=newXmlElement(XML_CONTEXT,"type:optional",XML_ELEM("ast:type",$2),XML_END); SOURCE(($$),(@$));  }

Typename: BuiltinType
| Qname Opt_TemplateArguments 
 { $$=newXmlElement(XML_CONTEXT,"type:typename",$1,XML_ELEM("type:generics",$2),XML_END); SOURCE(($$),(@$));  }

Typeof: TYPEOF '(' Expression ')' 
 { $$=newXmlElement(XML_CONTEXT,"type:typeof",XML_ELEM("ast:expression",$3),XML_END); SOURCE(($$),(@$)); }

TemplateArguments:  LT_OPERATOR List_TemplateArgument GT_OPERATOR 
 { $$=$2; }

TemplateArgument: Type
| Literal 
| '(' Expression ')'  
 { $$ = $2; }

BuiltinType:
TYPE_CHAR    
 { $$=newXmlElement(XML_CONTEXT,"type:character",XML_END); SOURCE(($$),(@$));  }
|TYPE_INTEGER 
 { $$=newXmlElement(XML_CONTEXT,"type:integer",XML_END); SOURCE(($$),(@$));  }
|TYPE_REAL    
 { $$=newXmlElement(XML_CONTEXT,"type:real",XML_END); SOURCE(($$),(@$));  }
|TYPE_DECIMAL 
 { $$=newXmlElement(XML_CONTEXT,"type:decimal",XML_END); SOURCE(($$),(@$));  }
|TYPE_STRING  
 { $$=newXmlElement(XML_CONTEXT,"type:string",XML_END); SOURCE(($$),(@$));  }
|TYPE_BIT     
 { $$=newXmlElement(XML_CONTEXT,"type:bit",XML_END); SOURCE(($$),(@$));  }
|TYPE_OCTET   
 { $$=newXmlElement(XML_CONTEXT,"type:octet",XML_END); SOURCE(($$),(@$));  }
|TYPE_BOOLEAN 
 { $$=newXmlElement(XML_CONTEXT,"type:boolean",XML_END); SOURCE(($$),(@$));  }

ArraySize: Expression

ArrayBound: '[' OptList_ArraySize ']' 
 { $$=newXmlElement(XML_CONTEXT,"type:array-bound",XML_ELEM("type:fixed",$2),XML_END); SOURCE(($$),(@$));  }

Member: Identifier ':' Type Opt_Initializer ';' 
 { $$=newXmlElement(XML_CONTEXT,"type:member",$1,XML_ELEM("ast:type",$3),XML_ELEM("type:initializer",$4),XML_END); SOURCE(($$),(@$));  }

Type: ArrayType | StreamType | ListType | Typeof;


Purity: SAFE 
 { $$=newXmlElement(XML_CONTEXT,"safe",XML_END);  SOURCE(($$),(@$));  }
| UNSAFE     
 { $$=newXmlElement(XML_CONTEXT,"unsafe",XML_END);  SOURCE(($$),(@$));  }
| PURE       
 { $$=newXmlElement(XML_CONTEXT,"pure",XML_END);  SOURCE(($$),(@$));  }

////////////////////////////////////////////////////////////////////////////
// Expressions
////////////////////////////////////////////////////////////////////////////
Argument: Expression                
 { $$=newXmlElement(XML_CONTEXT,"ast:argument",XML_ELEM("ast:expression",$1),XML_END); SOURCE(($$),(@$));  }
| Identifier ASSIGN Expression 
 { $$=newXmlElement(XML_CONTEXT,"ast:argument",$1,XML_ELEM("ast:expression",$3),XML_END); SOURCE(($$),(@$));  }

ArgumentList: '(' OptList_Argument ')' 
 { $$= newXmlElement(XML_CONTEXT,"ast:arguments",$2, XML_END);  SOURCE(($$),(@$));  }

Expression: ConditionalExpression

ConditionalExpression: LogicalExpression 
| LogicalExpression '?' Expression ':' Expression 
 { $$=newXmlElement(XML_CONTEXT,"expr:conditional",XML_ELEM("expr:condition",$1),XML_ELEM("expr:iftrue",$3),XML_ELEM("expr:iffalse",$5),XML_END); SOURCE(($$),(@$));  }

LogicalExpression: LogicalTerm
| LogicalTerm LOGICAL_OR_OPERATOR LogicalExpression  
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","||"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| LogicalTerm LOGICAL_XOR_OPERATOR LogicalExpression  
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","^^"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }

LogicalTerm: ComparisonExpression 
| ComparisonExpression LOGICAL_AND_OPERATOR LogicalTerm  
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","&&"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }

ComparisonExpression: ArithmeticExpression 
| ArithmeticExpression LT_OPERATOR ComparisonExpression  
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","<"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| ArithmeticExpression GT_OPERATOR ComparisonExpression  
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator",">"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| ArithmeticExpression LE_OPERATOR ComparisonExpression  
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","<="),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| ArithmeticExpression GE_OPERATOR ComparisonExpression  
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator",">="),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| ArithmeticExpression EQ_OPERATOR ComparisonExpression  
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","=="),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| ArithmeticExpression NE_OPERATOR ComparisonExpression  
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","!="),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }

ArithmeticExpression: ArithmeticTerm
| ArithmeticTerm PLUS_OPERATOR ArithmeticExpression 
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","+"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| ArithmeticTerm MINUS_OPERATOR ArithmeticExpression 
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","-"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| ArithmeticTerm SHIFT_LEFT_OPERATOR ArithmeticExpression 
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","<<"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| ArithmeticTerm SHIFT_RIGHT_OPERATOR ArithmeticExpression 
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator",">>"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| ArithmeticTerm ARITHMETIC_SHIFT_RIGHT_OPERATOR ArithmeticExpression 
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator",">>>"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }

ArithmeticTerm: BitwiseExpression
| BitwiseExpression MUL_OPERATOR ArithmeticTerm 
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","*"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| BitwiseExpression DIV_OPERATOR ArithmeticTerm 
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","/"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| BitwiseExpression MOD_OPERATOR ArithmeticTerm 
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","%"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }

BitwiseExpression: BitwiseTerm
| BitwiseTerm BITWISE_OR_OPERATOR BitwiseExpression 
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","|"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }
| BitwiseTerm BITWISE_XOR_OPERATOR BitwiseExpression 
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","^"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }

BitwiseTerm: UnaryExpression
| UnaryExpression BITWISE_AND_OPERATOR BitwiseTerm 
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator","&"),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }

UnaryExpression: ReferenceExpression 
| MINUS_OPERATOR ReferenceExpression       
 { $$ = newXmlElement(XML_CONTEXT,"expr:unary-expression",XML_TEXT_ELEM("expr:operator","arithmeticNot"),XML_ELEM("ast:expression",$2),XML_END); SOURCE(($$),(@$));  }
| BITWISE_NOT_OPERATOR ReferenceExpression 
 { $$ = newXmlElement(XML_CONTEXT,"expr:unary-expression",XML_TEXT_ELEM("expr:operator","bitwiseNot"),XML_ELEM("ast:expression",$2),XML_END); SOURCE(($$),(@$));  }
| LOGICAL_NOT_OPERATOR ReferenceExpression 
 { $$ = newXmlElement(XML_CONTEXT,"expr:unary-expression",XML_TEXT_ELEM("expr:operator","logicalNot"),XML_ELEM("ast:expression",$2),XML_END); SOURCE(($$),(@$));  }

ReferenceExpression: CastExpression
| MUL_OPERATOR CastExpression 
 { $$ = newXmlElement(XML_CONTEXT,"expr:unary-expression",XML_TEXT_ELEM("expr:operator","*"),XML_ELEM("ast:expression",$2),XML_END); SOURCE(($$),(@$));  }

CastExpression: InterpolateExpression
| CAST LT_OPERATOR Type  GT_OPERATOR '(' Expression ')'  
 { $$ = newXmlElement(XML_CONTEXT,"expr:type-cast",XML_TEXT_ELEM("kind","safe"),XML_ELEM("expr:type",$3),XML_ELEM("ast:expression",$6),XML_END); SOURCE(($$),(@$));  }
| LOSSY_CAST LT_OPERATOR Type  GT_OPERATOR '(' Expression ')'  
 { $$ = newXmlElement(XML_CONTEXT,"expr:type-cast",XML_TEXT_ELEM("kind","unsafe"),XML_ELEM("expr:type",$3),XML_ELEM("ast:expression",$6),XML_END); SOURCE(($$),(@$));  }

InterpolateExpression: RangeExpression
| BEGIN_TEXT OptList_InterpolatedText END_TEXT 
 { $$=newXmlElement(XML_CONTEXT,"expr:interpolate",XML_ELEM("ast:expressions",$2),XML_END); SOURCE(($$),(@$));  }

InterpolatedText: LiteralString { $$=XML_ELEM("ast:expression",$1); SOURCE(($$),(@$)); }
| BEGIN_EXPR Expression END_EXPR 
 { $$=XML_ELEM("ast:expression",$2); SOURCE(($$),(@$)); }


RangeExpression: ValueExpression
| ValueExpression RANGE ValueExpression 
 { $$ = newXmlElement(XML_CONTEXT,"expr:binary-expression",XML_TEXT_ELEM("expr:operator",".."),XML_ELEM("expr:lhs",$1),XML_ELEM("expr:rhs",$3),XML_END); SOURCE(($$),(@$));  }

ValueExpression: FunctionCall
| Literal
| ArrayLiteral
| '(' Expression ')'  
 { $$ = $2; }
| Constructor

FunctionCall: AssignableExpression  
| FunctionCall ArgumentList                               
 { $$ = newXmlElement(XML_CONTEXT,"expr:call",XML_ELEM("expr:callee",$1),$2,XML_END); SOURCE(($$),(@$));  }

/**
 * Assignable expressions ultimately boil down to the reference to a variable 
 */
AssignableExpression: LookupNameExpression 
| AssignableExpression '[' List_Expression ']' 
 { $$ = newXmlElement(XML_CONTEXT,"expr:index",XML_ELEM("expr:source",$1),XML_ELEM("expr:indexes",$3),XML_END); SOURCE(($$),(@$));  }
| AssignableExpression '.' Identifier 
 { $$ = newXmlElement(XML_CONTEXT,"expr:select",XML_ELEM("expr:source",$1),$3,XML_END); SOURCE(($$),(@$));  }

LookupNameExpression: Identifier
 { $$= newXmlElement(XML_CONTEXT,"expr:lookup",$1,XML_END); SOURCE(($$),(@$)); }
| GENERIC ':' Identifier TemplateArguments 
 { $$ = newXmlElement(XML_CONTEXT,"expr:lookup",$3,$4,XML_END); SOURCE(($$),(@$));  }
| RESULT 
 { $$=newXmlElement(XML_CONTEXT,"expr:result",XML_END); SOURCE(($$),(@$)); }

Constructor: NEW Type ArgumentList     
 { $$ = newXmlElement(XML_CONTEXT,"expr:new",XML_ELEM("expr:type",$2),XML_ELEM("expr:arguments",$3),XML_END); SOURCE(($$),(@$));  }

ArrayLiteral : '[' OptList_Expression ']' 
 { $$=newXmlElement(XML_CONTEXT,"expr:array-literal",XML_ELEM("expr:elements",$2),XML_END); SOURCE(($$),(@$));  }

Literal: LiteralInteger|LiteralReal|LiteralString|LiteralCharacter|LiteralBoolean|LiteralBit|LiteralOctet|
BuiltinType '(' LiteralString ')' 
 { $$=newXmlElement(XML_CONTEXT,"expr:unparsed-literal",XML_ELEM("expr:value",$1),XML_ELEM("expr:value", $3),XML_END); SOURCE(($$),(@$));  }
LiteralInteger: LITERAL_INTEGER
 { $$=newXmlElement(XML_CONTEXT,"expr:literalInteger",XML_ELEM("expr:value",$1),XML_END);  SOURCE(($$),(@$));  }
LiteralReal:    LITERAL_REAL      
 { $$=newXmlElement(XML_CONTEXT,"expr:literalReal",XML_ELEM("expr:value",$1),XML_END); SOURCE(($$),(@$));  }

LiteralString:  LITERAL_STRING    
 { $$=newXmlElement(XML_CONTEXT,"expr:literalString",XML_ELEM("expr:value",$1),XML_END); SOURCE(($$),(@$));  }

LiteralCharacter: LITERAL_CHARACTER 
 { $$=newXmlElement(XML_CONTEXT,"expr:literalCharacter",XML_ELEM("expr:value",$1),XML_END); SOURCE(($$),(@$));  }

LiteralBoolean:  LITERAL_BOOLEAN     
 { $$=newXmlElement(XML_CONTEXT,"expr:literalBoolean",XML_ELEM("expr:value",$1),XML_END); SOURCE(($$),(@$));  }

LiteralBit: LITERAL_BIT           
 { $$=newXmlElement(XML_CONTEXT,"expr:literalBit",XML_ELEM("expr:value",$1),XML_END); SOURCE(($$),(@$));  }

LiteralOctet: LITERAL_OCTET       
 { $$=newXmlElement(XML_CONTEXT,"expr:literalOctet",XML_ELEM("expr:value",$1),XML_END); SOURCE(($$),(@$));  }

////////////////////////////////////////////////////////////////////////////
// optionals
////////////////////////////////////////////////////////////////////////////
OptList_Attribute:   { $$=0;  } | List_Attribute;
OptList_Statement:   { $$=0;  } | List_Statement;
OptList_Parameter:  { $$=0; } | List_Parameter;
OptList_Argument:  { $$=0; } | List_Argument;
OptList_Variable:  { $$=0;} | List_Variable;
OptList_Expression:  { $$=0;} | List_Expression;
OptList_SimpleStatement:  { $$=0;} | List_SimpleStatement;
OptList_Type:  { $$=0;} | List_Type;
OptList_InterpolatedText:  { $$=0;} | List_InterpolatedText;
OptList_ArraySize:  { $$=0;} | List_ArraySize;
Opt_Label:   { $$=0;  } | Label;
Opt_VariableType:  { $$=0; } | VariableType;
Opt_Initializer:  { $$=0; } | Initializer;
Opt_Else:  { $$=0;} | Else;
Opt_Purity:  { $$=0;} | Purity;
Opt_TemplateArguments:  { $$ = 0; } | TemplateArguments
Opt_Expression:  { $$=0; } | Expression
Opt_Details:  { $$=0; } | Details
Opt_MacroParameters:  { $$=0; } | MacroParameters

////////////////////////////////////////////////////////////////////////////
// list productions
////////////////////////////////////////////////////////////////////////////
List_Identifier: Identifier | List_Identifier '.' Identifier  { $$=XML_CONCAT($1,$3); }
List_Parameter: Parameter | List_Parameter ',' Parameter  { $$=XML_CONCAT($1,$3); }
List_Argument: Argument | List_Argument ',' Argument  { $$=XML_CONCAT($1,$3); }
List_Variable: Variable | List_Variable ',' Variable  { $$=XML_CONCAT($1,$3); }
List_ArraySize: ArraySize| List_ArraySize ',' ArraySize  { $$=XML_CONCAT($1,$3); }
List_Type: Type | List_Type ',' Type  { $$=XML_CONCAT($1,$3); }
List_TemplateArgument: TemplateArgument | List_TemplateArgument ',' TemplateArgument  { $$=XML_CONCAT($1,$3); }
List_SimpleStatement: SimpleStatement | List_SimpleStatement ',' SimpleStatement  { $$=XML_CONCAT($1,$3); }
List_TemplateParameter:  TemplateParameter | List_TemplateParameter ',' TemplateParameter  { $$=XML_CONCAT($1,$3); }

List_Attribute: Attribute | List_Attribute Attribute  { $$=XML_CONCAT($1,$2); }
List_Statement: Statement | List_Statement Statement  { $$=XML_CONCAT($1,$2); }
List_ArrayBound: ArrayBound | List_ArrayBound ArrayBound  { $$=XML_CONCAT($1,$2); }
List_Member: Member | List_Member Member  { $$=XML_CONCAT($1,$2); }
List_InterpolatedText: InterpolatedText | List_InterpolatedText InterpolatedText  { $$=XML_CONCAT($1,$2); }
List_SwitchCase:  SwitchCase | List_SwitchCase SwitchCase  { $$=XML_CONCAT($1,$2); }

List_Expression: Expression { $$=XML_ELEM("ast:expression",$1); } | List_Expression ',' Expression  { $$=XML_CONCAT($1,XML_ELEM("ast:expression",$3)); }

AltList_Type: Type | AltList_Type BITWISE_OR_OPERATOR Type  { $$=XML_CONCAT($1,$3); }
AltList_Literal: Literal | AltList_Literal BITWISE_OR_OPERATOR Literal  { $$=XML_CONCAT($1,$3); }

