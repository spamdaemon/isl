#include "bison.h"
#include <stdio.h>

struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} ;

extern struct YYLTYPE yylloc;

void yyerror (char const *s)
{
  fprintf(stderr,"Parse error: %d:%d %s\n",yylloc.last_line,yylloc.last_column,s);
}

XmlNode makeLocation(struct XmlContext* context, struct YYLTYPE* src)
{
	  XmlNode start = newXmlElement(context,"ast:start",
			  newXmlElement(context,"ast:line",newXmlUInt(context,src->first_line),XML_END),
			  newXmlElement(context,"ast:column",newXmlUInt(context,src->first_column),XML_END),
			  XML_END);
	  XmlNode end = newXmlElement(context,"ast:end",
			  newXmlElement(context,"ast:line",newXmlUInt(context,src->last_line),XML_END),
			  newXmlElement(context,"ast:column",newXmlUInt(context,src->last_column),XML_END),
			  XML_END);


  return newXmlElement(context,"ast:location",start,end,XML_END);
}
